# README #

This project is for testing parcel.js and other tool I have not tried before.

### What is this repository for? ###

* Getting up to speed on new build tools
* Version: 1.0.0

### How do I get set up? ###

To set up the repository:
```
npm install
```

To run the parcel test server:
```
npm start
```

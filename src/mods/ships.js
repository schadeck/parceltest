/**
 * common.js module
 */
module.exports = {
    core: {
        entryCall: 'https://swapi.co/api/starships/'
    },
    grab: function(url) {
        return fetch(url)
            .then(res => res.json())
            .then(data => {
                this.placeBlocks(data, this.core.grid);
                console.log(data);
                data.next ? this.grab(data.next) : document.querySelector('.loader').classList.add('off');
            });
    },
    placeBlocks: function(data, grid) {
        data.results.map(ship => {
            const templ = `<div class="block">
            <div class="block__title">${ship.name}</div>
            <div class="block__desc">${ship.starship_class}</div>
            </div>`;
            grid.insertAdjacentHTML('beforeend', templ);
        });
    },
    init: function() {
        this.core.grid = document.querySelector('.grid');
        this.grab(this.core.entryCall);
    }
};
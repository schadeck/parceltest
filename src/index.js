const ships = require('./mods/ships');
// use `fs` to read file from system
const fs = require('fs');

// run the ship module
ships.init();

// example of pulling the contents of a file into the DOM
const copy = fs.readFileSync(__dirname + '/mods/unrelated.txt', 'utf8');
console.log(copy);
document.querySelector('.unrelated').innerText = copy;